# Theory Ontology

Describes the data schema for the ProvocaTeach website in SHACL.

When in conflict with others, the `theory.trig` file should take precedence.